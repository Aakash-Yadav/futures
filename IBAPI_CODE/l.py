import requests
import time
import json

url = 'https://www.google.com/async/finance_wholepage_price_updates?ei=_1sNZYSqB9CF-Qbw4p3gBA&opi=89978449&sca_esv=567545457&yv=3&cs=1&async=mids:%2Fg%2F1dt_x762%7C%2Fg%2F1dv25gpy%7C%2Fg%2F1hbvlyws1%7C%2Fg%2F1dtxgdfv%7C%2Fg%2F1dtt6h7d%7C%2Fm%2F046k_p%7C%2Fm%2F04t5sp,currencies:,_fmt:jspb'

headers = {
    'authority': 'www.google.com',
    'accept': '*/*',
    'accept-language': 'en-GB,en;q=0.8',
    'referer': 'https://www.google.com/',
    'sec-ch-ua': '"Brave";v="117", "Not;A=Brand";v="8", "Chromium";v="117"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-model': '""',
    'sec-ch-ua-platform': 'Linux',
    'sec-ch-ua-platform-version': '6.2.0',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'sec-gpc': '1',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
}
from pprint import pprint

while True:
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        # Do something with the response here
        print("Request successful at", time.ctime())
        json_data = json.loads(response.text.split('\n')[1])
        with open('output.json', 'w') as f:
            json_str = json.dumps(json_data,indent=3)
            f.write(json_str)

    else:
        print(f"Request failed with status code: {response.status_code}")

    # Sleep for 2 seconds before making the next request
    time.sleep(10)
