from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract
from ibapi.order import Order
from random import randint
from threading import  Thread
from datetime import datetime

class MyWrapper(EClient, EWrapper):
    def __init__(self):
        EClient.__init__(self, self)
        self.order_id_random = 300
    
    def add_up(self):
        self.order_id_random = self.order_id_random + randint(1, 10)
        return self.order_id_random

    def contractDetails(self, reqId, contractDetails):
        print(f"contract details: {contractDetails}")

    def contractDetailsEnd(self, reqId):
        print("End of contractDetails")
        # self.disconnect()


    def placeMarketOrder(self):
        date_strings = [f'2023{x:02}' for x in range(1, 13) if x % 3 == 0]
        formatted_dates = [datetime.strptime(date, '%Y%m') for date in date_strings]
        day_now = datetime.now()
        first_greater_date = next(date for date in formatted_dates if date > day_now).strftime('%Y%m')
        contract = Contract()
        contract.symbol = "NQ"
        contract.secType = "FUT"
        contract.exchange = "CME"
        contract.currency = "USD"
        contract.lastTradeDateOrContractMonth = first_greater_date# December 2023
        contract.multiplier = 20
        
        # self.reqContractDetails()

        order = Order()
        
        order.action = "BUY"  # Replace with "SELL" if you want to sell.
        order.totalQuantity = 10  # Number of contracts you want to trade.
        order.orderType = "MKT"  # Market order type.
        
        order.lmtPrice = 0.5
        order.orderId = self.add_up()
        # order.goodTillDate = expiration_date_time.replace(tzinfo=timezone.utc).strftime("%Y%m%d-%H:%M:%S")
        order.eTradeOnly = False
        # order.filledQuantity = False
        order.eTradeOnly = False
        order.firmQuoteOnly = False
        order.transmit = True
        
        order.orderId = self.add_up()

        self.placeOrder(order.orderId, contract, order)

def main(client):
    client.connect("127.0.0.1", 7497, clientId=0)
    client.run()


def other(client):
    
    if client.isConnected():
        print("CONNECT")
        client.placeMarketOrder()

if __name__ == "__main__":
    client = MyWrapper()

    t1 = Thread(target=main,args=(client,))
    t1.start()
    from time import sleep
    sleep(2);
    t2 = Thread(target=other , args=(client,))
    t2.start()
    
