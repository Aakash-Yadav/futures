
# from .twsclient import  TwsApiClient
try:
    from .twsclient import TwsApiClient
    # from common import OptionOrder, logger, Tick, getExpiry
    # import common
    from ibapi.order import Order
    from . import common
    # import .common
except:
    from twsclient import TwsApiClient
    # from common import OptionOrder, logger, Tick, getExpiry
    # import common
    from ibapi.order import Order
    import common
    

from threading import Thread
from queue import Queue
import time
from pandas import DataFrame

time_intervals = [
    '1 min', '2 mins', '3 mins', '5 mins', '10 mins',
    '15 mins', '20 mins', '30 mins', '1 hour', '2 hours',
    '3 hours', '4 hours', '8 hours', '1 day', '1W', '1M'
]

time_dict = dict.fromkeys(time_intervals, None)

contract_session = {}

def return_the_order_object(client, Name, Quantity, Signal,type_t=True):

    mul = 50 if Name == "ES" else 20 
    stock_contract = client.get_future_contract(Name,mul)
        
    order = Order()
    order.action = Signal.upper()
    order.orderType = "MKT"
    # order.tif = "GTD"
    order.totalQuantity = Quantity
    order.lmtPrice = 0.5
    order.orderId = client.nextOrderId()
    # order.goodTillDate = expiration_date_time.replace(tzinfo=timezone.utc).strftime("%Y%m%d-%H:%M:%S")
    order.eTradeOnly = False
    # order.filledQuantity = False
    order.eTradeOnly = False
    order.firmQuoteOnly = False
    order.transmit = True

    # print(stock_contract, dir(stock_contract));

    return (order, stock_contract)




def init_data_feed(symbol, subscribe_list, client, trade_session=False):
    try:
        if not client.isConnected():
            common.logger.info("TWS not connected.")
            return
        stock_contracts = []


        if trade_session:
            mul = 50 if symbol == "ES" else 20 
            stock_contract = client.get_future_contract(symbol, mul)
            print(stock_contract)
        else:
            stock_contract = client.get_stock_contract(symbol)

        stock_contracts.append(stock_contract)
        
        print(stock_contract, 'f'*30)
        for stock_contract in stock_contracts:
            for time_intervals in subscribe_list:
                if time_dict[time_intervals] != True:
                    client.subscribe(contract=stock_contract)
                    client.subscribe_historical_data(
                        contract=stock_contract, fetchValue="1 W", barSize=time_intervals)
                    print("subscribe_historical_data "*5)
                    time_dict[time_intervals] = True
        time.sleep(3.0)
    except Exception as ex:
        common.logger.error(f"init_data_feed: {ex}")


def start_client(_client) -> None:
    print("calling start_client")
    # _order_mgr.set_client(client=_client)
    time.sleep(0.5)
    if _client is None or not _client.isConnected():
        common.logger.error("TWS not connected")
        return

    if _client is None or not _client.isConnected():
        common.logger.error("TWS not connected")
        return

    client_thread = Thread(target=_client.run)
    client_thread.start()


def return_the_client():
    Q = Queue()
    client = TwsApiClient(event_queue=Q, callback=None)
    client.connect(host="127.0.0.1", port=7497, clientId=100)
    return client


client = return_the_client()

# init_data_feed("ES",['3 mins'],client,True);
# print(return_the_order_object(client,"AAPL",3,"SELL"))

# from time import sleep
# sleep(3)
# print(client.reqHistoricalData(1, client.get_future_contract("ES"), "", "1 D", '1 min' , "TRADES", 0, 1, False, [], ))

# client.reqHistoricalData(1, tickContract, "", validDuration, self.candle, "TRADES", 0, 1, False, [], )


# print(client.get_bars(stock= "ES", barSize= '3 mins',limit=100))
