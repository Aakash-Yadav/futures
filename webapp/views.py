from flask import Blueprint,render_template,request,jsonify,session
from requests import post 
from . import cache
# import concurrent.futures
from functools import reduce
from IBAPI_CODE.my_code import return_the_order_object , client
import os
from .indic_calcu import name_of_indecator
from json import load 
from json import dumps

views = Blueprint("views" ,__name__);

headers = {
    "Content-Type": "application/json"
}


def fetch_data(api_url,name , json_value ):
    response = post(api_url, json=json_value, headers=headers)
    return response.json()

def save_file_json(value):
    with open("/home/akash/Desktop/Test/JsonLogs/information.json",'w') as f:
            f.write(f'{dumps(value,indent=2)}')

def read_json_file():
    if os.path.exists("/home/akash/Desktop/Test/JsonLogs/information.json"):    
        with open("/home/akash/Desktop/Test/JsonLogs/information.json",'r') as f:
            data = load(f)
        return data;
    else:
        return False
    

@views.route('/')
def home():
    return render_template('home.html')


@views.route('/get_information_',methods=['POST'])
def save_file():
    value = request.json 
    
    print("APIII CALLL")
    
    
    old_json_values = read_json_file();

    
    if isinstance(old_json_values,bool):
        value["C_O"] = "RSI_OPEN";
        value['indicator_all_key'] = name_of_indecator(value);
        session['RSI_OPEN_RSI_CLOSE'] = True
        session['The_order_valeus_signal'] = ""
        save_file_json(value)
        

    if isinstance(old_json_values,dict):
        
        if session['RSI_OPEN_RSI_CLOSE']:
            value["C_O"] = 'RSI_OPEN'
        else:
            value["C_O"] = 'RSI_CLOSE'
        
        if old_json_values["indicator_all_key"]['Trade'] == True:
            final_signal = name_of_indecator(value,previous=1,P_date=old_json_values["indicator_all_key"]['Dtime']);
            
            value['indicator_all_key'] = final_signal 
            
            
            ########## THIS CODE RUN THE RSI OPEN
            if final_signal['take_trade'] == True and session['RSI_OPEN_RSI_CLOSE']:
                print("PLACE ___ ORDERRRR "*100)
                print(f"{value['indicator_all_key']['SuperTrend_SIGNAL']}","RSI_OPEN");
                session['RSI_OPEN_RSI_CLOSE'] = False
                order_object = return_the_order_object(client=client,Name=value["stock_names"],Quantity=int(value['quantity_values']),
            Signal=  value['indicator_all_key']['SuperTrend_SIGNAL'])
                
                session['The_order_valeus_signal'] = (value['indicator_all_key']['SuperTrend_SIGNAL'])
                
                contract = order_object[-1];
                order_object = order_object[0];
                client.placeOrder(order_object.orderId, contract , order_object);
                
                return jsonify({"MSG":True})
                
            ########### THIS CODE RUN RSI CLOSE
            if session['RSI_OPEN_RSI_CLOSE'] == False and session['The_order_valeus_signal']!=value['indicator_all_key']['SuperTrend_SIGNAL']:
                
                print("PLACE ___ ORDERRRR "*100)
                print(f"{value['indicator_all_key']['SuperTrend_SIGNAL']}","RSI_CLOSE")
                
                session['RSI_OPEN_RSI_CLOSE'] = True;
                order_object = return_the_order_object(client=client,Name=value["stock_names"],Quantity=int(value['quantity_values'])
                ,Signal=value['indicator_all_key']['SuperTrend_SIGNAL'])
                
                contract = order_object[-1];
                order_object = order_object[0];
                client.placeOrder(order_object.orderId, contract , order_object);
                
                return jsonify({"MSG":False})

        elif old_json_values["indicator_all_key"]['Trade'] == False:
            value['indicator_all_key'] = name_of_indecator(value);
            
    # result = []
    
    
    # for values in data_of_ind:
    #      = values;
    #     data = fetch_data(api,json_value=value,name=values);
    #     result.append({value[values]:data})
    
    # sum_of_dict = reduce(lambda a,b:a|b,result);
    
    # value['indicator_all_key'] = name_of_indecator(value);

    
    # print(value)
    #       "SuperTrend_SIGNAL": "BUY",
    #   "Trade": true,
    # if value['indicator_all_key'][value['RSI_OPEN']]["Trade"]:
    
    # if os.path.exists('/home/akash/Desktop/Test/JsonLogs/information.json'):

    #     order_object = return_the_order_object(client=client,Name=value["stock_names"],Quantity=int(value['quantity_values']),Signal=  value['indicator_all_key'][value['RSI_OPEN']]['SuperTrend_SIGNAL'])
        
    #     # print(order_object);
        
    #     rsi_open_bool = value['indicator_all_key'][value['RSI_OPEN']]["Trade"]
    #     rsi_close_bool =  value['indicator_all_key'][value['RSI_CLOSE']]["Trade"]
        
    #     rsi_open_signal =  value['indicator_all_key'][value['RSI_OPEN']]["SuperTrend_SIGNAL"]
    #     rsi_close_signal = value['indicator_all_key'][value['RSI_CLOSE']]["SuperTrend_SIGNAL"]
        
    #     order_object = return_the_order_object(client=client,Name=value["stock_names"],Quantity=int(value['quantity_values']),Signal=rsi_open_signal)
    #     contract = order_object[-1];
    #     order_object = order_object[0];
    #     client.placeOrder(order_object.orderId, contract , order_object);
        
    
    save_file_json(value)
    return   jsonify({"MSG":"Sn20"})


@views.route('/close')
def close():
    client.disconnect()
    return jsonify({"DISCONNECT":True})

    