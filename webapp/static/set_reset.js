let intervalId;
let idNames = [
  "stock_names",
  "currentprice_values",
  "quantity_values",
  "sma_1",
  "sma_2",
  "sma_length",
  "Rsi_L",
  "Rsi_S",
  "RSI_OPEN",
  "RSI_CLOSE",
  "RSI_Length",
  "lossInput",
  "closeInput",
];
const html_options = {
  "1 min": "1",
  "2 mins": "2",
  "3 mins": "3",
  "5 mins": "5",
  "10 mins": "10",
  "15 mins": "15",
  "20 mins": "20",
  "30 mins": "30",
  "1 hour": "60",
  "2 hours": "120",
  "3 hours": "180",
  "4 hours": "240",
  "8 hours": "480",
  "1 day": "1440",
  "1W": "10080",
  "1M": "43200",
};

function print_live_candel_value() {
  const btn = document.getElementById("StOp");

  if (!btn.disabled) {
    const apiUrl3 = `http://localhost:5001/live_candle_values/${
      document.getElementById("stock_names").innerText
    }/${document.getElementById("RSI_OPEN").value}`;

    fetch(apiUrl3)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `Failed to fetch data. Status code: ${response.status}`
          );
        }
        return response.text();
      })
      .then((data) => {
        console.log(JSON.stringify( JSON.parse(data), null, 2));
      })
      .catch((error) => {
        console.error(`An error occurred: ${error.message}`);
      });
  }else{
    console.log("THE API IS OOF FOR NOW");
  }
}

function send_the_information_to_flask(data_to_send) {
  const api = "/get_information_";

  fetch(api, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data_to_send), // Make sure to stringify the JSON data
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data); // This line logs the JSON response to the console
      // Handle the response from the server
      if (data["name"] != true) {
        clearInterval(intervalId);
        startInterval(data["name"]);
      }
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

function set_values() {
  document.getElementById("quantity_values").value = 120;

  document.getElementById("sma_1").value = 1.5;
  document.getElementById("sma_2").value = 2.5;

  document.getElementById("sma_length").value = 14;
  document.getElementById("Rsi_L").value = 70;

  document.getElementById("Rsi_S").value = 30;
  document.getElementById("RSI_Length").value = 14;

  document.getElementById("lossInput").value = 200;
  document.getElementById("closeInput").value = 200;
}

function reset() {
  for (let i = 0; i < idNames.length; i++) {
    document.getElementById(idNames[i]).disabled = false;
  }
  set_values();
}

function disabled_all() {
  for (let i = 0; i < idNames.length; i++) {
    document.getElementById(idNames[i]).disabled = true;
  }
}

function submit() {
  let quantity = document.getElementById("quantity_values").value;
  let name = document.getElementById("stock_names").innerText;

  if ((Number(quantity) < 10 && name != "Stock") || name != "") {
    let values = {};

    // Loop through the array and gather values
    for (let id of idNames) {
      let element = document.getElementById(id);
      if (element) {
        values[id] = element.value || element.innerText;
      }
    }
    // console.log(values);

    values["indicators"] = indicator()[1];
    values["open_long_short"] = collectRadioValues("opEn_long");
    values["Date_Time"] = date_time();
    // values["type_to_of_trade"] = document.getElementById("inputGroup-sizing-default_1").innerText

    let inputValuesJSON = JSON.stringify(values);
    // console.log(inputValuesJSON);
    // console.log(JSON.stringify( JSON.parse(inputValuesJSON), null, 2))
    //   disabled_all()
    send_the_information_to_flask(inputValuesJSON);

    //   let ind_value = indicator_calls()
    //   console.log(ind_value['[[PromiseResult]]'])
  } else {
    alert("Quantity is grater than 10");
  }
}

function startInterval(n = "RSI_OPEN") {
  console.log(`STARTING WITH ${n}`);
  document.getElementById("StOp").disabled = false;
  document.getElementById("SUBmit").disabled = true;
  intervalId = setInterval(function () {
    submit();
  }, Number(html_options[document.getElementById(n).value]) * 60000);
}
//
// Number(html_options[document.getElementById("RSI_OPEN").value])*60000
function stop_the_session() {
  clearInterval(intervalId);
  clearInterval(intervalId_of_live);

  let api = "/close";

  fetch(api)
    .then((response) => {
      if (!response.ok) {
        throw new Error(
          `Network response was not ok, status code: ${response.status}`
        );
      }
      return response.json(); // Parse response body as JSON
    })
    .then((data) => {
      console.log("Fetched data:", data); // JSON data
      // You can process or use the data as needed
    })
    .catch((error) => {
      console.error("Fetch error:", error);
    });
  reset();
}

set_values();

// intervalId_of_live = setInterval(print_live_candel_value, 5000);