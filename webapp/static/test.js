let intervalId_of_live;

function myfun(n) {
  indec = document.getElementById(n);
  console.log(indec.value);
}

function changeDropdownValue(buttonId, newValue) {
  const button = document.getElementById(buttonId);
  button.innerText = newValue;
}

function addListItem(text) {
  let Btn_to_update = document.getElementById("Stock-names-to-change"); // Assuming you've already selected your ul element
  let li = document.createElement("li");
  let a = document.createElement("a");
  a.className = "dropdown-item";
  a.href = "#";
  a.textContent = text;
  a.setAttribute(
    "onclick",
    `changeDropdownValue_Of_Tiker('stock_names', '${text}')`
  );
  li.appendChild(a);
  Btn_to_update.appendChild(li);
}

// function toggleText() {
//   const button = document.getElementById("inputGroup-sizing-default_1");
//   let stock_names = document.getElementById("stock_names");
//   clearInterval(intervalId)
//   console.log(intervalId);
//   let stockNames = ["AAPL", "AMZN", "MSFT", "GOOGL", "FB"];
//   let fu_stockNames = ["NQ", "ES"];

//   let Btn_to_update = document.getElementById("Stock-names-to-change");

//   const currentText = button.innerText;
//   stock_names.innerText = "Stock";
//   if (currentText === "Equity") {
//     button.innerText = "Futures";
//     while (Btn_to_update.firstChild) {
//       Btn_to_update.removeChild(Btn_to_update.firstChild);
//     }
//     for (let i = 0; i < fu_stockNames.length; i++) {
//       addListItem(fu_stockNames[i]);
//     }
//   } else {
//     button.innerText = "Equity";
//     while (Btn_to_update.firstChild) {
//       Btn_to_update.removeChild(Btn_to_update.firstChild);
//     }
//     for (let i = 0; i < stockNames.length; i++) {
//       addListItem(stockNames[i]);
//     }
//   }
// }

function date_time() {
  const now = new Date();
  const hours = String(now.getHours()).padStart(2, "0");
  const minutes = String(now.getMinutes()).padStart(2, "0");
  const seconds = String(now.getSeconds()).padStart(2, "0");
  const time = `${hours}:${minutes}:${seconds}`;
  return time;
}

function indicator() {
  let selected_values = false;
  let indicator_on = [];
  let all_div_value = document.getElementById("inddd").childNodes;

  for (let i = 0; i < all_div_value.length; i++) {
    if (all_div_value[i].checked) {
      selected_values = true;
      indicator_on.push(all_div_value[i].id);
    }
  }

  return [selected_values, indicator_on];
}
// return [selected_values ,indicator_on];

function collectRadioValues() {
  let containerId = "opEn_long";
  let container = document.getElementById(containerId);

  if (!container) {
    console.error("Container not found");
    return {};
  }

  let radioButtons = container.querySelectorAll('input[type="radio"]');
  let valuesObject = {};

  radioButtons.forEach((radioButton) => {
    let radioButtonId = radioButton.id;
    let isChecked = radioButton.checked ? "on" : "off";

    valuesObject[radioButtonId] = isChecked;
  });

  return valuesObject;
}

function add_basic_info_at_top(name) {
  let apiUrl1;
  if (
    document.getElementById("inputGroup-sizing-default_1").innerText ==
    "Futures"
  ) {
    apiUrl1 = `/stock_basic_details/Stock`;
  } else {
    apiUrl1 = `/stock_basic_details/${name}`;
  }

  const apiUrl2 = "/count_of_trade";

  const fetch1 = fetch(apiUrl1).then((response) => response.json());

  const fetch2 = fetch(apiUrl2).then((response) => response.json());

  Promise.all([fetch1, fetch2])
    .then((data) => {
      const responseData1 = data[0];
      const dataFromUrl2 = data[1];

      // Now you can work with the fetched data

      document.getElementById("description_values").value =
        responseData1["Dec"];
      document.getElementById("currentprice_values").value =
        responseData1["current_price"];

      document.getElementById("trade_count").innerText =
        dataFromUrl2["trade_count"];
    })
    .catch((error) => {
      console.error("Error fetching data:", error);
    });
}

function changeDropdownValue_Of_Tiker(buttonId, newValue) {
  let select = indicator();
  if (select[0]) {
    const button = document.getElementById(buttonId);
    button.innerText = newValue;

    console.log(select);
    add_basic_info_at_top(newValue);
    document.getElementById("SUBmit").disabled = false;
    document.getElementById("StOp").disabled = true;
  }
}

document.getElementById("StOp").disabled = true;

setInterval(() => {
  let currentTickerValue = document.getElementById("stock_names").innerText;
  add_basic_info_at_top(currentTickerValue);
}, 1000);
