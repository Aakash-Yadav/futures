
from . import ind as Indecator
from IBAPI_CODE.my_code import init_data_feed, client, return_the_order_object
from pandas import DataFrame
import os
import pathlib 
from json import load, dumps

JSON_FILE = str(pathlib.Path().absolute() / 'JsonLogs' / 'information.json')

def save_file_json(value):
    
    
    with open(JSON_FILE, 'w') as f:
        f.write(f'{dumps(value,indent=2)}')


def read_json_file():
    if os.path.exists(JSON_FILE):
        with open(JSON_FILE, 'r') as f:
            data = load(f)
        return (True, data)
    else:
        return (0, 0)


def take_order(values):
    order_object = return_the_order_object(
    client=client,
    Name=values['stock_names'],
    Quantity=int(values['quantity_values']),
    Signal=values['indicator_all_key']['SuperTrend_SIGNAL'],type_t=False)
    contract = order_object[-1]
    order_object = order_object[0]
    client.placeOrder(order_object.orderId, contract, order_object)
    return True


def name_of_indicator(Json_data,previous=0,P_date=None):

    name = (Json_data['stock_names'])

    close_open = Json_data['C_O']

    interval = (Json_data[close_open])
    Sma_length = int(Json_data['sma_length'])


    init_data_feed(client=client , subscribe_list=[Json_data['RSI_OPEN'], Json_data['RSI_CLOSE']] ,symbol=name,trade_session=True) 

    live_bar_values  =(client.get_bars(stock= name, barSize= Json_data[close_open],limit=100))

    Dict_frame = {
        "Close" :   [x.close for x in live_bar_values],
        "High":     [x.high for x in live_bar_values],
        "Low":      [x.low for x in live_bar_values],
        "DateTime": [x.date for x in live_bar_values]
    }
    data = DataFrame(Dict_frame);
    print(data)
    # value = ['supertrend','Atr','Vwap','support & resistance','alpha trend','Ema(8-13-21)']

    supertrend = Indecator.BOTSingal(data, interval=interval)
    print_column = supertrend[["Close","ST_BUY_SELL","DateTime"]]
    print(print_column)
    supertrend_send = supertrend['ST_BUY_SELL'].iloc[-1]

    the_old_json_file_signal = read_json_file()


    print( [ supertrend['DateTime'].iloc[-1] , supertrend['DateTime'].iloc[-2] , supertrend['DateTime'].iloc[-3]][::-1])
    print('\n');
    print( [ supertrend['ST_BUY_SELL'].iloc[-1] , supertrend['ST_BUY_SELL'].iloc[-2] , supertrend['ST_BUY_SELL'].iloc[-3]][::-1])


    take_trade = False

    if the_old_json_file_signal[0]:
        # Extract the old signal from the JSON file
        the_old_signal = the_old_json_file_signal[-1]['indicator_all_key']['SuperTrend_SIGNAL']

        # Determine whether to send a true/false signal
        supertrend_send_true_false = (supertrend['ST_BUY_SELL'].iloc[-1] != the_old_signal or
            supertrend['ST_BUY_SELL'].iloc[-1] != supertrend['ST_BUY_SELL'].iloc[-2])
    else:
        # Determine whether to send a true/false signal when the JSON file signal is not present
        supertrend_send_true_false = supertrend['ST_BUY_SELL'].iloc[-1] != supertrend['ST_BUY_SELL'].iloc[-2] and supertrend['ST_BUY_SELL'].iloc[-1] != supertrend['ST_BUY_SELL'].iloc[-3];


    if previous:
        
        last_signal_live   =supertrend['ST_BUY_SELL'].iloc[-1];
        second_signal_live =supertrend['ST_BUY_SELL'].iloc[-2];
        third_signal_live  =supertrend['ST_BUY_SELL'].iloc[-3];
        fourth_signal_live =supertrend['ST_BUY_SELL'].iloc[-4];
        

        if (last_signal_live == second_signal_live == third_signal_live) and (fourth_signal_live!=third_signal_live):
            take_trade = True;
        
        if (last_signal_live == second_signal_live and
            second_signal_live != third_signal_live and
            last_signal_live != third_signal_live):
            take_trade = True

    sma = Indecator.SMA(data,  days=Sma_length)
    sma_send = sma['SMA'].iloc[-1]

    rsi = Indecator.getRSICalculate(data, n=int(
        Json_data['RSI_Length']))

    rsi_send = rsi['RSI'].iloc[-1]

    # EMA (8-13-21)

    EMA_8_13_21 = Indecator.EMA_8_13_21(interval=interval, pdData=data)
    send_ema = {"8": EMA_8_13_21[0].iloc[-1], '13': EMA_8_13_21[1].iloc[-1],
                '21': EMA_8_13_21[-1].iloc[-1]}

    return_value = {"SuperTrend_SIGNAL": supertrend_send, "SMA": sma_send, "EMA": send_ema,
                    "interval": interval, "Trade": supertrend_send_true_false, "RSI": rsi_send,"Dtime":"%s"%(supertrend['DateTime'].iloc[-1]),"take_trade":take_trade}
    return (return_value)