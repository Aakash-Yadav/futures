from flask import Flask
from flask_caching import Cache
from flask_cors import CORS

app = Flask(__name__);
CORS(app);

app.config['CACHE_TYPE'] = 'simple'  # You can use 'redis', 'memcached', etc.

app.secret_key = 'your_secure_random_key_here'

cache = Cache(app)

from .views_2 import views 


app.register_blueprint(views,url_prefix='/');
