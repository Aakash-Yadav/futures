from flask import Blueprint, render_template, request, jsonify, session
from requests import post
from . import cache
# import concurrent.futures
from functools import reduce
from yfinance import Ticker
# from IBAPI_CODE.my_code import return_the_order_object, client  
import os
from .indic_calcu import *
import datetime


views = Blueprint("views", __name__)


@views.route('/')
def home():
    cache.set('trade_counter', 0, timeout=7_200)
    print("TRADE COUNT HAS BEEN SET")
    return render_template('home.html')


@views.route('/count_of_trade')
def count_of_trade():

    the_counter = cache.get('trade_counter')

    if count_of_trade is None:
        return jsonify({"trade_count": 0})
    else:
        return jsonify({'trade_count': the_counter});

@views.route('/stock_basic_details/<name>')
def stock_basic_details(name: str):
    current_time = datetime.datetime.now().time().strftime("%H:%M:%S")
    if name == "Stock":
        return jsonify({"current_price": None, "Dec": current_time})
    stock = Ticker(name)
    current_price = round(stock.history().tail(1)["Close"].iloc[0], 2)
    current_time = datetime.datetime.now().time().strftime("%H:%M:%S")
    return jsonify({"current_price": "%.2f" % (current_price),
                    "Dec": "(%s) (%s) %.2f" % (name, current_time, current_price)})




@views.route('/get_information_',methods=['POST'])
def save_file():

    value = eval(request.json);
    
    print(value['stock_names'])
    
    time_cache_data = cache.get('cached_time');
    print(type(value))
    if time_cache_data is None:
        value['C_O'] = "RSI_OPEN";
        cache.set('cached_time', "RSI_OPEN" ,  timeout=3600);
    else:
        value['C_O'] = time_cache_data;
    
    
    print("APIII CALLL")
    
    print(value['C_O'])
    
    old_json_values = read_json_file();
    
    if old_json_values[0]:
        
        the_all_old_values_of_live = old_json_values[-1];
        
        if the_all_old_values_of_live["indicator_all_key"]['Trade'] == True:
        # if the_all_old_values_of_live["indicator_all_key"]['Trade'] == True or value['C_O'] == "RSI_CLOSE":
            """ A TRADE SIGNAL IS TRUE WAITING FOR THE NEXT Confirmation """
            
            final_signal = name_of_indicator(value,previous=1);
            
            value['indicator_all_key'] = final_signal; 
            
            if value['C_O'] == "RSI_OPEN" and final_signal['take_trade'] == True:
                cache.set('cached_time', "RSI_CLOSE" ,  timeout=5600);
                
                the_signal = value['indicator_all_key']['SuperTrend_SIGNAL'];
                
                save_signal = {"RSI_OPEN":the_signal}
                
                cache.set('signal_of_rsi',save_signal, timeout=7_200);
                
                take_order(values=value);
                
                the_counter = cache.get('trade_counter');the_counter+=1;cache.set('trade_counter', the_counter, timeout=7_200)
                
                return jsonify({"name":"RSI_CLOSE"})
            
            if value['C_O'] == "RSI_CLOSE" and (
                final_signal['SuperTrend_SIGNAL'] != 
                cache.get('signal_of_rsi')['RSI_OPEN']):
                
                cache.set('cached_time', "RSI_OPEN" ,  timeout=5600);
        
                take_order(values=value);
                    
                the_counter = cache.get('trade_counter');the_counter+=1;cache.set('trade_counter', the_counter, timeout=7_200)
                
                return jsonify({"name":"RSI_OPEN"})
            
            if final_signal['take_trade'] == False:
                save_file_json(value)
        else:
            value['indicator_all_key'] = name_of_indicator(value);
            save_file_json(value)
            
    if not old_json_values[0]:
        value['indicator_all_key'] = name_of_indicator(value);
        save_file_json(value)
        
    return   jsonify({"name": True})



# @views.route('/get_information_', methods=['POST'])
# def save_file():

#     # Parse the JSON data from the request
#     request_data = eval(request.json)

#     # Extract the 'stock_names' value from the request data
#     stock_names = request_data.get('stock_names')
#     print(stock_names)

#     # Get the 'cached_time' value from the cache, if it exists
#     time_cache_data = cache.get('cached_time')
#     print(type(request_data))

#     # Set 'C_O' in the request data based on the cached time or default to "RSI_OPEN"
#     if time_cache_data is None:
#         request_data['C_O'] = "RSI_OPEN"
#         cache.set('cached_time', "RSI_OPEN", timeout=3600)
#     else:
#         request_data['C_O'] = time_cache_data

#     print("API CALL")

#     # Print the value of 'C_O'
#     print(request_data['C_O'])

#     # Read old JSON values from a file
#     old_json_values = read_json_file()

#     if old_json_values[0]:

#         # Get the most recent set of values from old JSON data
#         the_all_old_values_of_live = old_json_values[-1]

#         if the_all_old_values_of_live["indicator_all_key"]['Trade'] == True:
#             # A TRADE SIGNAL IS TRUE, WAITING FOR THE NEXT Confirmation

#             # Calculate the final signal based on some function 'name_of_indicator'
#             final_signal = name_of_indicator(request_data, previous=1)

#             # Update 'indicator_all_key' in the request data
#             request_data['indicator_all_key'] = final_signal

#             if request_data['C_O'] == "RSI_OPEN" and final_signal['take_trade'] == True:
#                 # Set 'cached_time' to "RSI_CLOSE" and save the RSI_OPEN signal
#                 cache.set('cached_time', "RSI_CLOSE", timeout=5600)
#                 save_signal = {"RSI_OPEN": request_data['indicator_all_key']['SuperTrend_SIGNAL']}
#                 cache.set('signal_of_rsi', save_signal, timeout=7_200)
#                 take_order(values=request_data)
#                 the_counter = cache.get('trade_counter')
#                 the_counter += 1
#                 cache.set('trade_counter', the_counter, timeout=7_200)
#                 return jsonify({"name": "RSI_CLOSE"})

#             if request_data['C_O'] == "RSI_CLOSE" and (
#                     final_signal['SuperTrend_SIGNAL'] !=
#                     cache.get('signal_of_rsi')['RSI_OPEN']):

#                 # Set 'cached_time' to "RSI_OPEN" and take an order
#                 cache.set('cached_time', "RSI_OPEN", timeout=5600)
#                 take_order(values=request_data)
#                 the_counter = cache.get('trade_counter')
#                 the_counter += 1
#                 cache.set('trade_counter', the_counter, timeout=7_200)
#                 return jsonify({"name": "RSI_OPEN"})

#             if final_signal['take_trade'] == False:
#                 save_file_json(request_data)
#         else:
#             request_data['indicator_all_key'] = name_of_indicator(request_data)
#             save_file_json(request_data)

#     if not old_json_values[0]:
#         request_data['indicator_all_key'] = name_of_indicator(request_data)
#         save_file_json(request_data)

#     return jsonify({"name": True})



@views.route('/close')
def close():
    client.disconnect()
    return jsonify({"DISCONNECT": True})
