from flask import Flask, jsonify, request, render_template
from yfinance import Ticker, download
import datetime
from flask_cors import CORS
import yfinance as yf
from IBAPI_CODE.my_code import init_data_feed,client
from json import dumps,load

# from . import ind as Indecator

api_app = Flask(__name__)


CORS(api_app)

def histDataframe(ticker, period="5d", interval='1m'):
    sym = yf.Ticker(ticker)
    sym_data = sym.history(period=period, interval=interval, actions=False)
    return sym_data

def return_the_live_candle(name:str,time_frame:str):
    time_frame = time_frame.replace("'", '')
    init_data_feed(client=client , subscribe_list=[time_frame] ,symbol=name);
    live_bar_values  =(client.get_bars(stock= name, barSize= time_frame));
    the_return_dict  = {
        "Close" :   [x.close for x in live_bar_values],
        "High":     [x.high for x in live_bar_values],
        "Low":      [x.low for x in live_bar_values],
        "DateTime": [x.date for x in live_bar_values],
    }
    return the_return_dict
    
@api_app.route('/live_candle_values/<name>/<time_frame>')
def live_candle_provider(name, time_frame):
    print(time_frame)
    the_ruturn =  return_the_live_candle(name=name,time_frame=time_frame);
    return  jsonify(the_ruturn);


@api_app.route('/stock_basic_details/<name>')
def stock_basic_details(name: str):
    current_time = datetime.datetime.now().time().strftime("%H:%M:%S")
    if name == "Stock":
        return jsonify({"current_price": None, "Dec": current_time})
    stock = Ticker(name)
    current_price = round(stock.history().tail(1)["Close"].iloc[0], 2)
    current_time = datetime.datetime.now().time().strftime("%H:%M:%S")
    return jsonify({"current_price": "%.2f" % (current_price),
                    "Dec": "(%s) (%s) %.2f" % (name, current_time, current_price)})



@api_app.route('/candel_data')
def home():
    return render_template("home.html")